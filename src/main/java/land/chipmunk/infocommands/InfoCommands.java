package land.chipmunk.infocommands;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.metadata.ModMetadata;
import net.fabricmc.loader.api.metadata.Person;
import net.minecraft.server.command.ServerCommandSource;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.word;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import net.minecraft.text.Text;
import net.minecraft.text.MutableText;
import net.minecraft.util.Formatting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Optional;
import java.util.Collection;

public class InfoCommands implements ModInitializer {
  // public static final Logger LOGGER = LoggerFactory.getLogger("infocommands");
  private static final DynamicCommandExceptionType UNKNOWN_MOD_EXCEPTION = new DynamicCommandExceptionType(modid -> Text.literal("Unable to find mod '").append(String.valueOf(modid)).append(Text.literal("'")));

  @Override
  public void onInitialize() {
  }

  public static void registerCommands (CommandDispatcher<ServerCommandSource> dispatcher) {
    final LiteralCommandNode modsNode = dispatcher.register(literal("mods").executes(InfoCommands::listMods));
    dispatcher.register(literal("plugins").executes(InfoCommands::listMods).redirect(modsNode));
    dispatcher.register(literal("pl").executes(InfoCommands::listMods).redirect(modsNode));

    final LiteralCommandNode versionNode = dispatcher.register(
      literal("version")
        .executes(InfoCommands::sendVersion)
        .then(
          argument("mod", word())
            .executes(InfoCommands::sendModInfo)
        )
    );
    dispatcher.register(literal("ver").executes(InfoCommands::sendVersion).redirect(versionNode));
    dispatcher.register(literal("about").executes(InfoCommands::sendVersion).redirect(versionNode));
  }

  public static int listMods (CommandContext<ServerCommandSource> context) {
    final MutableText output = Text.literal("Mods: ");

    for (ModContainer container : FabricLoader.getInstance().getAllMods()) {
      if (container.getContainingMod().isPresent()) continue; // ignore nested mods
      final ModMetadata metadata = container.getMetadata();
      if (metadata.getType().equals("builtin")) continue;
      if (output.getSiblings().size() != 0) output.append(Text.literal(", "));

      String name = metadata.getName();
      if (name == null) name = metadata.getId();

      output.append(Text.literal(name).formatted(Formatting.GREEN));
    }

    context.getSource().sendFeedback(() -> output, false);
    return Command.SINGLE_SUCCESS;
  }

  public static int sendVersion (CommandContext<ServerCommandSource> context) {
    final FabricLoader loader = FabricLoader.getInstance();

    final ModContainer minecraftContainer = loader.getModContainer("minecraft").get();
    final ModContainer loaderContainer = loader.getModContainer("fabricloader").get();

    final Text text = Text.literal("This server is running Fabric Loader version ")
      .append(Text.literal(loaderContainer.getMetadata().getVersion().toString()))
      .append(Text.literal(" (on MC "))
      .append(Text.literal(minecraftContainer.getMetadata().getVersion().toString()))
      .append(Text.literal(")"));

    context.getSource().sendFeedback(() -> text, false);

    return Command.SINGLE_SUCCESS;
  }

  public static int sendModInfo (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final FabricLoader loader = FabricLoader.getInstance();

    final String modid = getString(context, "mod");
    final Optional<ModContainer> optional = loader.getModContainer(modid);
    if (!optional.isPresent()) throw UNKNOWN_MOD_EXCEPTION.create(modid);

    final ModContainer container = optional.get();
    final ModMetadata metadata = container.getMetadata();

    final MutableText output = Text.literal(metadata.getName())
      .append(Text.literal(" version "))
      .append(Text.literal(metadata.getVersion().toString()));

    final String description = metadata.getDescription();
    if (!description.isEmpty()) {
      output.append(Text.literal("\n"));
      output.append(Text.literal(description));
    }

    final Collection<Person> authors = metadata.getAuthors();
    if (authors.size() != 0) {
      final MutableText list = Text.literal("\nAuthors: ");

      for (Person person : authors) {
        if (list.getSiblings().size() != 0) list.append(Text.literal(", ").formatted(Formatting.GRAY));
        list.append(Text.literal(person.getName()));
      }

      output.append(list);
    }

    context.getSource().sendFeedback(() -> output, false);

    return Command.SINGLE_SUCCESS;
  }
}
